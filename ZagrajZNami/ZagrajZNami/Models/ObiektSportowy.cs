﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ZagrajZNami.Models
{
    public class ObiektSportowy
    {
        //Keys
        public int ObiektSportowyID { get; set; }
        public int KategoriaID { get; set; }
        public int AdresID { get; set; }

        [Required(ErrorMessage = "Wprowadź Nazwe Obiektu")]
        [StringLength(100, ErrorMessage = "Nazwa nie moze byc dluzsza niz 100 znakow")]
        public string Nazwa { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Wprowadź Date Dodania")]
        [CustomValidation(typeof(ObiektSportowy), "SprawdzPoprawnoscDatyDodania")]
        public DateTime DataDodania { get; set; }


        public static ValidationResult SprawdzPoprawnoscDatyDodania(DateTime data)
        {
            if (data.Date > DateTime.Now.Date)
                return new ValidationResult("Data Dodania nie może być późniejsza od daty dzisiejsze");
            else
                return ValidationResult.Success;
        }

        [Required(ErrorMessage = "Wprowadź Opis Obiektu")]
        [StringLength(350, ErrorMessage = "Nazwa nie moze byc dluzsza niz 350 znakow")]
        public string OpisObiektu { get; set; }

        public bool Wolny { get; set; }

        [Required(ErrorMessage = "Wprowadź Maksymalną Liczbe Miejsc")]
        [Range(0,100, ErrorMessage = "Maksymalna liczba miejsc nie może być większa niż 100")]
        public int MaksymalnaLiczbaMiejsc { get; set; }

        [Required(ErrorMessage = "Wprowadź Minimalna Liczbe Miejsc, ażeby spotkanie się odbyło")]
        [Range(0, 100, ErrorMessage = "Minimalna liczba miejsc nie może być  niż 100")]
        public int MinimalnaLiczbaMiejsc { get; set; }

        [Range(0, 50000, ErrorMessage = "Szerokosc nie moze byc wieksza niz 50000")]
        public int Szerokosc { get; set; }

        [Range(0, 50000, ErrorMessage = "Dlugosc nie moze byc wieksza niz 50000")]
        public int Dlugosc { get; set; }
        
        //Właściwości nie podlegające kontroli
        public string NazwaPlikuObrazka { get; set; }
        public string Test { get; set; }

        //virutal for leazy loading which means that EF Will only load data when try to access them
        public virtual Kategoria Kategoria { get; set; }
        public virtual Adres Adres { get; set; }
        public virtual ICollection<Zdjecie> Zdjecia { get; set; }




    }
}