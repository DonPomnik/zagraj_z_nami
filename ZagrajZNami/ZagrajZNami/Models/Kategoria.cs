﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ZagrajZNami.Models
{
    public class Kategoria
    {
        public int KategoriaID { get; set; }

        [Required(ErrorMessage = "Wprowadź Nazwe Obiektu")]
        [StringLength(100, ErrorMessage = "Nazwa nie moze byc dluzsza niz 100 znakow")]
        public string NazwaKategorii { get; set; }

        //Właściwości nie podlegające kontroli
        public string OpisKategorii { get; set; }
        public string NazwaPlikuIkony { get; set; }

        public virtual ICollection<ObiektSportowy> ObiektySportowe { get; set; }
    }
}