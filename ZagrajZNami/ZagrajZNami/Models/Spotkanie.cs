﻿  using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZagrajZNami.Models
{
    [ValidateInput(false)]
    public class Spotkanie
    {
        public int SpotkanieID { get; set; }
        public int ObiektSportowyID { get; set; }
        public string UserId { get; set; }

        [Required(ErrorMessage = "Wprowadź Godzine Rozpoczecia")]
        [RegularExpression(@"^(?:[01]\d|2[0-3]):[0-5]\d$", ErrorMessage = "Invalid Time.")]
        public string GodzinaRozpoczecia { get; set; }

        [Required(ErrorMessage = "Wprowadź Godzine Zakończenia")]
        [RegularExpression(@"^(?:[01]\d|2[0-3]):[0-5]\d$", ErrorMessage = "Invalid Time.")]
        public string GodzinaZakonczenia { get; set; }

        [DataType(DataType.Date)]
        public DateTime DataSpotkania { get; set; }


        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Wprowadź Date Dodania")]
        public DateTime DataDodania { get; set; }

        public string GlownyObrazek { get; set; }

        //public static ValidationResult SprawdzPoprawnoscDatyDodania(DateTime data)
        //{
        //    if (data.Date > DateTime.Now.Date)
        //        return new ValidationResult("Data Dodania nie może być późniejsza od daty dzisiejsze");
        //    else
        //        return ValidationResult.Success;
        //}

        public static ValidationResult IsValidTimeFormat(string input)
        {
            TimeSpan dummyOutput;
            if(TimeSpan.TryParse(input, out dummyOutput))
                return ValidationResult.Success;
            else
                return new ValidationResult("Błędny format czasu");
        }


        [Required(ErrorMessage = "Wprowadź Opis Spotkania")]
        [StringLength(350, ErrorMessage = "Opis nie moze byc dluzsza niz 350 znakow")]
        [AllowHtml]
        
        public string OpisSpotkania { get; set; }

        public string Z { get; set; }

        [Required(ErrorMessage = "Wprowadź Maksymalną Liczbe Miejsc")]
        [Range(0, 10000, ErrorMessage = "Cena nie może być większa niż 10000")]
        public decimal Cena { get; set; }

        //Właściwości nie podlegające kontroli
        public bool Anulowany { get; set; }
        public int LiczbaMiejscZajetych { get; set; }

        public virtual ObiektSportowy ObiektSportowy { get; set; }
        public virtual ICollection<ApplicationUser> ApplicationUsers { get; set; }
    }
}