﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZagrajZNami.Models
{
    public class Zdjecie
    {
        public int ZdjecieID { get; set; }
        public string Sciezka { get; set; }
        public int ObiektSportowyID { get; set; }

        public virtual ObiektSportowy ObiektSportowy { get; set; }
    }
}