﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ZagrajZNami.Models
{
    /// <summary>
    /// the point is that if you imagine to have two addresses for the same user in the future, 
    /// you should split now and have an address table with a FK pointing back to the users table.
    /// </summary>
    public class Adres
    {
        public int AdresID { get; set; }

        [Required(ErrorMessage = "Wprowadź Nazwe Ulicy")]
        [StringLength(100, ErrorMessage = "Nazwa Ulicy nie moze byc dluzszy niz 100 znakow")]
        public string Ulica { get; set; }

        [Required(ErrorMessage = "Wprowadź Kod Pocztowy")]
        [RegularExpression(@"\d{2}-\d{3}",ErrorMessage = "Podano niepoprawny format kodu pocztowego")]
        public string KodPocztowy { get; set; }

        [Required(ErrorMessage = "Wprowadź Numer")]
        [StringLength(10)]
        public string Numer { get; set; }

        [Required(ErrorMessage = "Wprowadź Nazwe Miasta")]
        [StringLength(200, ErrorMessage = "Nazwa Miasta nie moze byc dluzszy niz 200 znakow")]
        public string Miasto { get; set; }
    }
}