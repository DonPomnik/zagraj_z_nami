﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace ZagrajZNami.App_Start
{
    public class BundleConfig
    {
        /// <summary>
        /// Bundle == Usuniecie przestrzeni i komentarzy. Dodatkowo zawrzenie kilka styli lub skryptów w jeden bundle
        /// </summary>
        /// <param name="bundles"></param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                                        "~/Content/bootstrap.min.css",
                                        "~/Content/Footer-Clean.css",
                                        "~/Content/Login-Form-Clean.css",
                                        "~/Content/Navigation-with-Search.css",
                                        "~/Content/Simple-Slider.css",
                                        "~/Content/Footer-with-social-media-icons.css",
                                        "~/Content/font-awesome.min.css",
                                        "~/Content/Google-Style-Login.css",
                                        "~/Content/Login-Center.css",
                                        "~/Content/sticky-dark-top-nav-with-dropdown.css",
                                        "~/Content/styles.css",
                                        "~/fonts/font-awesome.min.css",
                                        "~/Content/Paralax-Hero-Banner-1.css",
                                        "~/Content/cards.css",
                                        "~/Content/Pretty-Registration-Form.css",
                                        "~/Content/Team.css"
                                        ));


            bundles.Add(new ScriptBundle("~/bundles/jqueryAndjqueryUI").Include(
                              "~/Scripts/jquery-{version}.js",
                              "~/Scripts/bootstrap.min.js",
                              "~/Scripts/Simple-Slider.js",
                              "~/Scripts/Paralax-Hero-Banner.js",
                              "~/Scripts/jquery-ui-{version}.js"));
        }
    }
}