namespace ZagrajZNami.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ZmianapolaliczbamiejscWolnych : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Spotkanie", "LiczbaMiejscWolnych", c => c.Int(nullable: false));
            DropColumn("dbo.ObiektSportowy", "LiczbaMiejscWolnych");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ObiektSportowy", "LiczbaMiejscWolnych", c => c.Int(nullable: false));
            DropColumn("dbo.Spotkanie", "LiczbaMiejscWolnych");
        }
    }
}
