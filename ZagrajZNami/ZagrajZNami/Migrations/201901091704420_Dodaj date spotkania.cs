namespace ZagrajZNami.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dodajdatespotkania : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Spotkanie", "DataSpotkania", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Spotkanie", "DataSpotkania");
        }
    }
}
