namespace ZagrajZNami.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ZmianapolaliczbamiejsZajetych : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Spotkanie", "LiczbaMiejscZajetych", c => c.Int(nullable: false));
            DropColumn("dbo.Spotkanie", "LiczbaMiejscWolnych");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Spotkanie", "LiczbaMiejscWolnych", c => c.Int(nullable: false));
            DropColumn("dbo.Spotkanie", "LiczbaMiejscZajetych");
        }
    }
}
