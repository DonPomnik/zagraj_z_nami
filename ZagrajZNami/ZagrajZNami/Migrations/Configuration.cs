namespace ZagrajZNami.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using ZagrajZNami.DAL;

    public sealed class Configuration : DbMigrationsConfiguration<ZagrajZNami.DAL.ZagrajZNamiContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ZagrajZNami.DAL.ZagrajZNamiContext";
        }

        protected override void Seed(ZagrajZNami.DAL.ZagrajZNamiContext context)
        {
            //  This method will be called after migrating to the latest version.
            ZagrajZNamiInitializer.SeedZagrajZNamiData(context);
            ZagrajZNamiInitializer.SeedUzytkownicy(context);
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
