namespace ZagrajZNami.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ds : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Spotkanie", "Z", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Spotkanie", "Z");
        }
    }
}
