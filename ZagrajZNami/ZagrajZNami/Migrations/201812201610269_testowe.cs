namespace ZagrajZNami.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testowe : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ObiektSportowy", "Test", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ObiektSportowy", "Test");
        }
    }
}
