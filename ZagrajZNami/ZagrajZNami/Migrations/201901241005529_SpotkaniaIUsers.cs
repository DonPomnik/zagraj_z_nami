namespace ZagrajZNami.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SpotkaniaIUsers : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Spotkanie", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Spotkanie", new[] { "ApplicationUser_Id" });
            CreateTable(
                "dbo.ApplicationUserSpotkanie",
                c => new
                    {
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                        Spotkanie_SpotkanieID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ApplicationUser_Id, t.Spotkanie_SpotkanieID })
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id, cascadeDelete: true)
                .ForeignKey("dbo.Spotkanie", t => t.Spotkanie_SpotkanieID, cascadeDelete: true)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.Spotkanie_SpotkanieID);
            
            AddColumn("dbo.Spotkanie", "UserId", c => c.String());
            DropColumn("dbo.Spotkanie", "ApplicationUser_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Spotkanie", "ApplicationUser_Id", c => c.String(maxLength: 128));
            DropForeignKey("dbo.ApplicationUserSpotkanie", "Spotkanie_SpotkanieID", "dbo.Spotkanie");
            DropForeignKey("dbo.ApplicationUserSpotkanie", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.ApplicationUserSpotkanie", new[] { "Spotkanie_SpotkanieID" });
            DropIndex("dbo.ApplicationUserSpotkanie", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.Spotkanie", "UserId");
            DropTable("dbo.ApplicationUserSpotkanie");
            CreateIndex("dbo.Spotkanie", "ApplicationUser_Id");
            AddForeignKey("dbo.Spotkanie", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
