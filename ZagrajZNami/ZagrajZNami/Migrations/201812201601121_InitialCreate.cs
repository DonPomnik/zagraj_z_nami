namespace ZagrajZNami.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Adres",
                c => new
                    {
                        AdresID = c.Int(nullable: false, identity: true),
                        Ulica = c.String(nullable: false, maxLength: 100),
                        KodPocztowy = c.String(nullable: false),
                        Numer = c.String(nullable: false, maxLength: 10),
                        Miasto = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.AdresID);
            
            CreateTable(
                "dbo.Kategoria",
                c => new
                    {
                        KategoriaID = c.Int(nullable: false, identity: true),
                        NazwaKategorii = c.String(nullable: false, maxLength: 100),
                        OpisKategorii = c.String(),
                        NazwaPlikuIkony = c.String(),
                    })
                .PrimaryKey(t => t.KategoriaID);
            
            CreateTable(
                "dbo.ObiektSportowy",
                c => new
                    {
                        ObiektSportowyID = c.Int(nullable: false, identity: true),
                        KategoriaID = c.Int(nullable: false),
                        AdresID = c.Int(nullable: false),
                        Nazwa = c.String(nullable: false, maxLength: 100),
                        DataDodania = c.DateTime(nullable: false),
                        OpisObiektu = c.String(nullable: false, maxLength: 350),
                        Wolny = c.Boolean(nullable: false),
                        MaksymalnaLiczbaMiejsc = c.Int(nullable: false),
                        MinimalnaLiczbaMiejsc = c.Int(nullable: false),
                        Szerokosc = c.Int(nullable: false),
                        Dlugosc = c.Int(nullable: false),
                        LiczbaMiejscWolnych = c.Int(nullable: false),
                        NazwaPlikuObrazka = c.String(),
                    })
                .PrimaryKey(t => t.ObiektSportowyID)
                .ForeignKey("dbo.Adres", t => t.AdresID, cascadeDelete: true)
                .ForeignKey("dbo.Kategoria", t => t.KategoriaID, cascadeDelete: true)
                .Index(t => t.KategoriaID)
                .Index(t => t.AdresID);
            
            CreateTable(
                "dbo.Zdjecie",
                c => new
                    {
                        ZdjecieID = c.Int(nullable: false, identity: true),
                        Sciezka = c.String(),
                        ObiektSportowyID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ZdjecieID)
                .ForeignKey("dbo.ObiektSportowy", t => t.ObiektSportowyID, cascadeDelete: true)
                .Index(t => t.ObiektSportowyID);
            
            CreateTable(
                "dbo.Spotkanie",
                c => new
                    {
                        SpotkanieID = c.Int(nullable: false, identity: true),
                        ObiektSportowyID = c.Int(nullable: false),
                        GodzinaRozpoczecia = c.String(nullable: false),
                        GodzinaZakonczenia = c.String(nullable: false),
                        DataDodania = c.DateTime(nullable: false),
                        OpisSpotkania = c.String(nullable: false, maxLength: 350),
                        Cena = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Anulowany = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SpotkanieID)
                .ForeignKey("dbo.ObiektSportowy", t => t.ObiektSportowyID, cascadeDelete: true)
                .Index(t => t.ObiektSportowyID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Spotkanie", "ObiektSportowyID", "dbo.ObiektSportowy");
            DropForeignKey("dbo.Zdjecie", "ObiektSportowyID", "dbo.ObiektSportowy");
            DropForeignKey("dbo.ObiektSportowy", "KategoriaID", "dbo.Kategoria");
            DropForeignKey("dbo.ObiektSportowy", "AdresID", "dbo.Adres");
            DropIndex("dbo.Spotkanie", new[] { "ObiektSportowyID" });
            DropIndex("dbo.Zdjecie", new[] { "ObiektSportowyID" });
            DropIndex("dbo.ObiektSportowy", new[] { "AdresID" });
            DropIndex("dbo.ObiektSportowy", new[] { "KategoriaID" });
            DropTable("dbo.Spotkanie");
            DropTable("dbo.Zdjecie");
            DropTable("dbo.ObiektSportowy");
            DropTable("dbo.Kategoria");
            DropTable("dbo.Adres");
        }
    }
}
