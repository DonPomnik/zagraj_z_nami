﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZagrajZNami.Models;

namespace ZagrajZNami.ViewModels
{
    public class GlownaViewModel
    {
        public List<Spotkanie> Spotkania { get; set; }
        public List<ObiektSportowy> ObiektySportowe { get; set; }
    }
}