﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZagrajZNami.Models;

namespace ZagrajZNami.ViewModels
{
    public class SpotkanieObiektViewModels
    {
        public Spotkanie Spotkanie { get; set; }
        public ObiektSportowy ObiektSportowy { get; set; }
    }
}