﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZagrajZNami.Models;

namespace ZagrajZNami.ViewModels
{
    public class EmailViewModel
    {
        public ApplicationUser UserToSend { get; set; }
        public string Wiadomosc { get; set; }
    }
}