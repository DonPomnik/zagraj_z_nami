﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ZagrajZNami.App_Start;
using ZagrajZNami.DAL;
using ZagrajZNami.Models;
using static ZagrajZNami.ViewModels.ManageViewModels;

namespace ZagrajZNami.Controllers
{
    [RequireHttps]
    [Authorize]
    public class ManageController : Controller
    {
        private ZagrajZNamiContext db = new ZagrajZNamiContext();

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            Error
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


        // GET: Manage
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            if (TempData["ViewData"] != null)
            {
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }

            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("Index", "Admin");
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return View("Error");
            }

            var model = new ManageCredentialsViewModel
            {
                Message = message,
                DaneUzytkownika = user.DaneUzytkownika
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeProfile([Bind(Prefix = "DaneUzytkownika")]DaneUzytkownika daneUzytkownika)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                user.DaneUzytkownika = daneUzytkownika;
                var result = await UserManager.UpdateAsync(user);

                AddErrors(result);
            }

            if (!ModelState.IsValid)
            {
                TempData["ViewData"] = ViewData;
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword([Bind(Prefix = "ChangePasswordViewModel")]ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                TempData["ViewData"] = ViewData;
                return RedirectToAction("Index");
            }

            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInAsync(user, isPersistent: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);

            if (!ModelState.IsValid)
            {
                TempData["ViewData"] = ViewData;
                return RedirectToAction("Index");
            }

            var message = ManageMessageId.ChangePasswordSuccess;
            return RedirectToAction("Index", new { Message = message });
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("password-error", error);
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie, DefaultAuthenticationTypes.TwoFactorCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, await user.GenerateUserIdentityAsync(UserManager));
        }


        public ActionResult Spotkania()
        {
            var userID = User.Identity.GetUserId();
            var spotkania = db.Spotkania.Where(a => a.UserId == userID).ToList();
            return View(spotkania);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Spotkania(Spotkanie obiekt)
        {
            Regex checktime = new Regex(@"^(?:[01]\d|2[0-3]):[0-5]\d$");

            if (!checktime.IsMatch(obiekt.GodzinaRozpoczecia) || !checktime.IsMatch(obiekt.GodzinaZakonczenia))
            {
                ViewBag.Innfo = "Nie poprawny format godziny";
                var userID = User.Identity.GetUserId();
                var spotkania = db.Spotkania.Where(a => a.UserId == userID).ToList();
                return PartialView("_SpotkaniaList", spotkania);
            }
            //if (!ModelState.IsValid)
            //{
            //    var userID = User.Identity.GetUserId();
            //    var spotkania = db.Spotkania.Where(a => a.UserId == userID).ToList();
            //    return PartialView("_SpotkaniaList", spotkania);
            //}
            else
            {
                db.Entry(obiekt).State = EntityState.Modified;
                db.SaveChanges();
                var userID = User.Identity.GetUserId();
                var spotkania = db.Spotkania.Where(a => a.UserId == userID).ToList();
                return PartialView("_SpotkaniaList", spotkania);
            }
        }
    }
}