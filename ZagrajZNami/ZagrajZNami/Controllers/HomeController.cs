﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZagrajZNami.DAL;
using ZagrajZNami.Models;
using ZagrajZNami.ViewModels;

namespace ZagrajZNami.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        private ZagrajZNamiContext db = new ZagrajZNamiContext();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // GET: Home
        public ActionResult Index(string c = null)
        {
            logger.Info("wywołanie na stronie głównej");
            var listaSpotkan = db.Spotkania.OrderByDescending(a => Guid.NewGuid()).Take(3).ToList();
            var listaObiektow = db.ObiektySportowe.OrderByDescending(a => Guid.NewGuid()).Take(3).ToList();

            GlownaViewModel vm = new GlownaViewModel()
            {
                ObiektySportowe = listaObiektow,
                Spotkania  = listaSpotkan
            };

            if (c != null)
                ViewBag.Infooo = c;
            return View(vm);
        }

        public ActionResult Boiska()
        {
            var obiekty = db.ObiektySportowe.ToList();
            //return View(obiekty);
            return View("Index");
        }

        public ActionResult ONas()
        {
            return View();
        }
    }
}