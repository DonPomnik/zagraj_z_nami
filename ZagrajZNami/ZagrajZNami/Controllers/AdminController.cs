﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ZagrajZNami.App_Start;
using ZagrajZNami.DAL;
using ZagrajZNami.Models;
using ZagrajZNami.ViewModels;

namespace ZagrajZNami.Controllers
{
    [RequireHttps]
    [Authorize]
    public class AdminController : Controller
    {
        private ZagrajZNamiContext db = new ZagrajZNamiContext();

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            Error
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        // GET: Admin
        public ActionResult Index()
        {
            if (User.IsInRole("Admin"))
            {
                var spotkania = db.Spotkania.ToList();
                return View(spotkania);
            }
            else
                return RedirectToAction("Index", "Home");
                
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Spotkania(Spotkanie obiekt)
        {
            Regex checktime = new Regex(@"^(?:[01]\d|2[0-3]):[0-5]\d$");
            if (!checktime.IsMatch(obiekt.GodzinaRozpoczecia) || !checktime.IsMatch(obiekt.GodzinaZakonczenia))
            {
                ViewBag.Innfo = "Nie poprawny format godziny";
            }
            else
            {
                db.Entry(obiekt).State = EntityState.Modified;
                db.SaveChanges();
            }

            var spotkania = db.Spotkania.ToList();
            return PartialView("_SpotkaniaList", spotkania);
        }

        public ActionResult Uzytkownicy()
        {
            if (User.IsInRole("Admin"))
            {
                var users = db.Users.ToList();
                return View(users);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Uzytkownicy(ApplicationUser user, FormCollection collection = null)
        {
            if (User.IsInRole("Admin"))
            {
                var User = db.Users.Where(a => a.Email == user.Email).FirstOrDefault();
                User.DaneUzytkownika.Imie = collection["Imie"].ToString() != String.Empty ? collection["Imie"] : User.DaneUzytkownika.Imie;
                User.DaneUzytkownika.Nazwisko = collection["Nazwisko"].ToString() != String.Empty ? collection["Nazwisko"] : User.DaneUzytkownika.Nazwisko;
                User.DaneUzytkownika.Adres = collection["Adres"].ToString() != String.Empty ? collection["Adres"] : User.DaneUzytkownika.Adres;
                User.DaneUzytkownika.Miasto = collection["Miasto"].ToString() != String.Empty ? collection["Miasto"] : User.DaneUzytkownika.Miasto;
                User.DaneUzytkownika.KodPocztowy = collection["KodPocztowy"].ToString() != String.Empty ? collection["KodPocztowy"] : User.DaneUzytkownika.KodPocztowy;
                User.DaneUzytkownika.Telefon = collection["Telefon"].ToString() != String.Empty ? collection["Telefon"] : User.DaneUzytkownika.Telefon;

                db.Entry(User).State = EntityState.Modified;
                db.SaveChanges();
                var users = db.Users.ToList();
                return PartialView("_UzytkownicyList", users);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult UsunUzytkownika(string userID = null)
        {
            var user = db.Users.Where(a => a.Id == userID).FirstOrDefault();
            db.Entry(user).State = EntityState.Deleted;
            db.SaveChanges();
            var users = db.Users.ToList();
            return PartialView("_UzytkownicyList", users);
        }

        public ActionResult WyslijEmail()
        {
            if (User.IsInRole("Admin"))
            {
                var users = db.Users.ToList();
                EmailViewModel vm = new EmailViewModel();

                List<string> maileDb = new List<string>();
                db.Users.ToList().ForEach(a =>
                {
                    if (!a.Email.Equals(String.Empty))
                        maileDb.Add(a.Email);
                });

                ViewBag.Mails = maileDb;
                return View(vm);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> WyslijEmail(EmailViewModel vm)
        {
            ApplicationUser user = db.Users.Where(a => a.Email.ToLower() == vm.UserToSend.Email.ToLower()).FirstOrDefault();
            await UserManager.SendEmailAsync(user.Id, "Wiadomość od administratora strony zagrajznami.pl", vm.Wiadomosc);
            var users = db.Users.ToList();
            EmailViewModel vm1 = new EmailViewModel();

            List<string> maileDb = new List<string>();
            db.Users.ToList().ForEach(a =>
            {
                if (!a.Email.Equals(String.Empty))
                    maileDb.Add(a.Email);
            });

            ViewBag.Mails = maileDb;
            ViewBag.Info = "Wiadomość wysłana";
            return View(vm1);
        }

        [HttpPost]
        public ActionResult Zaktualizuj(string nazwa)
        {
            var spotkania = db.Spotkania.ToList();

            if (nazwa == "DataW")
            {
                spotkania = spotkania.OrderByDescending(a => a.DataDodania).ToList();
                return View("~/Views/Admin/_SpotkaniaList", spotkania);
            }

            else if (nazwa == "DataN")
            {
                spotkania = spotkania.OrderBy(a => a.DataDodania).ToList();
                return View("~/Views/Admin/_SpotkaniaList", spotkania);
            }

            else if (nazwa == "CenaNajt")
            {
                spotkania = spotkania.OrderBy(a => a.Cena).ToList();
                return View("~/Views/Admin/_SpotkaniaList", spotkania);
            }

            else if (nazwa == "CenaNajd")
            {
                spotkania = spotkania.OrderByDescending(a => a.Cena).ToList();
                return View("~/Views/Admin/_SpotkaniaList", spotkania);
            }
            return null;
        }
    }
}