﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZagrajZNami.App_Start;
using ZagrajZNami.DAL;
using ZagrajZNami.Infrastucture;
using ZagrajZNami.Models;
using ZagrajZNami.ViewModels;

namespace ZagrajZNami.Controllers
{
    [RequireHttps]
    public class SpotkanieController : Controller
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                //Najpierw dodaæ Nuget For Solution -> Broswe -> Owin Host System web
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ZagrajZNamiContext db = new ZagrajZNamiContext();


        // GET: Spotkanie
        public ActionResult Index()
        {
           var spotkanie = db.Spotkania.ToList();
           return View(spotkanie);
        }

        // GET: Spotkanie
        public string Index2(string OpisSpotkania, string Cena)
        {
            //string qr = String.Format("select * from AspNetUsers where email='jakub4742@gmail.com' and PasswordHash='{0}'",password);
            //var user = db.Database.SqlQuery<ApplicationUser>(qry).ToList();
            var Spotkania =  db.Spotkania.SqlQuery("Select * from Spotkanie where OpisSpotkania = '" + OpisSpotkania +
                                                "' and Cena = " + Cena).ToList();

            if (Spotkania != null)
            {
                string s = "";
                foreach (var item in Spotkania)
                {
                    s += item.OpisSpotkania + item.Cena + "|";
                }
                return s;
            }
            else
            {
                return null;
            }
            //var adpt = new SqlDataAdapter(qry, con);

            //var  dt = new DataTable();

            //adpt.Fill(dt);

            //if (dt.Rows.Count >= 1)
            //{
            //    return adpt.ToString();
            //}
            //else
            //{
            //    return "Nic nie ma";
            //}
        }
    
        [HttpPost]
        public ActionResult Zaktualizuj(string nazwa)
        {
            var spotkania = db.Spotkania.ToList();

            if (nazwa == "DataW")
            {
                spotkania = spotkania.OrderByDescending(a => a.DataDodania).ToList();
                return View("_SpotkaniaList", spotkania);
            }

            else if (nazwa == "DataN")
            {
                spotkania = spotkania.OrderBy(a => a.DataDodania).ToList();
                return View("_SpotkaniaList", spotkania);
            }

            else if (nazwa == "CenaNajt")
            {
                spotkania = spotkania.OrderBy(a => a.Cena).ToList();
                return View("_SpotkaniaList", spotkania);
            }

            else if (nazwa == "CenaNajd")
            {
                spotkania = spotkania.OrderByDescending(a => a.Cena).ToList();
                return View("_SpotkaniaList", spotkania);
            }
            return null;
        }

    
        public ActionResult DodajSpotkanie()
        {
            //if(!User.Identity.IsAuthenticated)
            //{
            //    string infoS = "przed dodaniem spotkania proszę się zalogować";
            //    return RedirectToAction("Login", "Account", new { info = infoS });
            //}

            var vm = new SpotkanieObiektViewModels()
            {
                ObiektSportowy = new ObiektSportowy(),
                Spotkanie = new Spotkanie()
            };

            var spotkanie = new Spotkanie();
            var listaSpotkan = db.Spotkania.ToList();
            var obiektySportowe = db.ObiektySportowe.ToList();

            //Zrob cache
            List<ObiektSportowy> WszystkieObiekty;
            ICacheProvider cache = new DefaultCacheProvider();

            if (cache.IsSet(Consts.SpotkaniaCache))
            {
                WszystkieObiekty = cache.Get(Consts.SpotkaniaCache) as List<ObiektSportowy>;
            }
            else
            {
                WszystkieObiekty = db.ObiektySportowe.ToList();
                //Ustaw na cały dzień bo admin bedzie aktualizował te obiekty co dzień
                cache.Set(Consts.SpotkaniaCache, WszystkieObiekty, 1440);
            }

            List<string> obiektyOb = new List<string>();
            obiektySportowe.ForEach(a =>
            {
                if (!a.Nazwa.Equals(String.Empty))
                    obiektyOb.Add(a.Nazwa);
            });

            ViewBag.NazwyObiektow = obiektyOb.Distinct();
            return View(vm);
        }

        [HttpPost]
        public ActionResult UsunZSpotkania(int spotkanieID = -1, string userID = null)
        {
            ApplicationUser user;
            List<Spotkanie> spotkania;

            if (userID != null)
            {
                user = db.Users.Where(a => a.Email == userID).FirstOrDefault();
                spotkania = db.Spotkania.Where(a => a.UserId == user.Id).ToList();
            }
            else
            {
                userID = User.Identity.GetUserId();
                spotkania = db.Spotkania.ToList();
                user = db.Users.Find(userID);
            }

            user.Spotkania.Remove(db.Spotkania.First(a => a.SpotkanieID == spotkanieID));
            db.Spotkania.Find(spotkanieID).ApplicationUsers.Remove(db.Users.Find(userID));
            if(db.Spotkania.Find(spotkanieID).LiczbaMiejscZajetych > 0)
                db.Spotkania.Find(spotkanieID).LiczbaMiejscZajetych--;
            db.SaveChanges();

            return View("_SpotkaniaList", spotkania);

        }

        [ValidateAntiForgeryToken]
        public ActionResult DolaczDoSpotkania(Spotkanie obiekt, string userId = null)
        {
            var user = UserManager.FindById(userId);

            if (!user.Spotkania.Any(a => a.SpotkanieID == obiekt.SpotkanieID))
            {
                db.Users.Find(userId).Spotkania.Add(db.Spotkania.First(a => a.SpotkanieID == obiekt.SpotkanieID));
                db.Spotkania.Find(obiekt.SpotkanieID).ApplicationUsers.Add(db.Users.Find(userId));
                db.Spotkania.Find(obiekt.SpotkanieID).LiczbaMiejscZajetych++;
                db.SaveChanges();
                var spotkania = db.Spotkania.ToList();
                return View("_SpotkaniaList", spotkania);
            }
            else
                return null;
           
        }


        [HttpPost]
        
        public ActionResult DodajSpotkanie(SpotkanieObiektViewModels vm)
        {
            //  var userID = User.Identity.GetUserId();
            var userID = "8ffa541d-42aa-44b6-a533-4799fcce6557";
            var listaSpotkan = db.Spotkania.ToList();
            var obiektySportowe = db.ObiektySportowe.ToList();
            ICacheProvider cache = new DefaultCacheProvider();

            //Zrob cache
            List<ObiektSportowy> WszystkieObiekty;

            if (cache.IsSet(Consts.SpotkaniaCache))
            {
                WszystkieObiekty = cache.Get(Consts.SpotkaniaCache) as List<ObiektSportowy>;
            }
            else
            {
                WszystkieObiekty = db.ObiektySportowe.ToList();
                //Ustaw na cały dzień bo admin bedzie aktualizował te obiekty co dzień
                cache.Set(Consts.SpotkaniaCache, WszystkieObiekty, 1440);
            }



            List<string> obiektyOb = new List<string>();
            obiektySportowe.ForEach(a =>
            {
                if (!a.Nazwa.Equals(String.Empty))
                    obiektyOb.Add(a.Nazwa);
            });

            ViewBag.NazwyObiektow = obiektyOb.Distinct();

            vm.Spotkanie.DataDodania = DateTime.Now;
            vm.Spotkanie.ObiektSportowyID = db.ObiektySportowe.Where(a => a.Nazwa == vm.ObiektSportowy.Nazwa).First().ObiektSportowyID;
            vm.Spotkanie.DataSpotkania = DateTime.Now;
            vm.Spotkanie.GlownyObrazek = "Obrazek5.jpg";
            vm.Spotkanie.UserId = userID;
            db.Spotkania.Add(vm.Spotkanie);
            db.SaveChanges();
            ViewBag.Dodano = "Poprawnie dodano spotkanie!";
            return RedirectToAction("DodajSpotkanie");
        }

    }
}