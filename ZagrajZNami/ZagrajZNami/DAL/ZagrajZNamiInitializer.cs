﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using ZagrajZNami.Migrations;
using ZagrajZNami.Models;

namespace ZagrajZNami.DAL
{
    public class ZagrajZNamiInitializer : MigrateDatabaseToLatestVersion<ZagrajZNamiContext,Configuration>
    {
        public static void SeedZagrajZNamiData(ZagrajZNamiContext context)
        {
            var adresy = new List<Adres>
            {
                new Adres(){AdresID = 1 , Miasto = "Puck", KodPocztowy = "84-100", Numer = "3/8",Ulica = "Szkolna"},
                new Adres(){AdresID = 2 , Miasto = "Gdynia", KodPocztowy = "84-100", Numer = "3/8",Ulica = "Szkolna"},
                new Adres(){AdresID = 3 , Miasto = "Gdańsk", KodPocztowy = "84-100", Numer = "3/8",Ulica = "Szkolna"},
                new Adres(){AdresID = 4 , Miasto = "Gdańsk", KodPocztowy = "84-100", Numer = "3/8",Ulica = "Szkolna"},
            };
            adresy.ForEach(a => context.Adresy.AddOrUpdate(a));
            context.SaveChanges();

            var kategorie = new List<Kategoria>
            {
                new Kategoria() {KategoriaID = 1 , NazwaKategorii = "Trawa Naturalna", NazwaPlikuIkony = "Trawka.png",OpisKategorii = "Boiska na naturalnej trawie"},
                new Kategoria() {KategoriaID = 2 , NazwaKategorii = "Trawa Sztuczna", NazwaPlikuIkony = "Sztuczna.png",OpisKategorii = "Boiska na sztucznej"},
                new Kategoria() {KategoriaID = 3 , NazwaKategorii = "Hala", NazwaPlikuIkony = "Hala.png",OpisKategorii = "Boiska na hali w zamkniętych pomieszczeniach"},
                new Kategoria() {KategoriaID = 4 , NazwaKategorii = "Plaza", NazwaPlikuIkony = "Plaza.png",OpisKategorii = "Boiska na plaży"},
                new Kategoria() {KategoriaID = 5 , NazwaKategorii = "Murawa", NazwaPlikuIkony = "Murawa.png",OpisKategorii = "Boiska na murawie"},
            };

            kategorie.ForEach(a => context.Kategorie.AddOrUpdate(a));
            context.SaveChanges();

            var obiektySportowe = new List<ObiektSportowy>
            {
                new ObiektSportowy() {ObiektSportowyID = 1 , KategoriaID = 1 , AdresID = 3 , Nazwa = "BoiskoArki",
                DataDodania = DateTime.Now.Date , OpisObiektu = "Boisko przeznaczone do gry dla osób powyżej 10 roku życia",
                MaksymalnaLiczbaMiejsc = 10 , MinimalnaLiczbaMiejsc = 5 , Szerokosc = 100 , Dlugosc = 100,
                NazwaPlikuObrazka = "Arka.jpg"},
                new ObiektSportowy() {ObiektSportowyID = 2 , KategoriaID = 4 , AdresID = 1 , Nazwa = "Boisko Plazowe koło morza",
                DataDodania = DateTime.Now.Date , OpisObiektu = "Boisko przyjmujące tylko specjlane typy piłek",
                MaksymalnaLiczbaMiejsc = 12 , MinimalnaLiczbaMiejsc = 6 , Szerokosc = 50 , Dlugosc = 50,
                NazwaPlikuObrazka = "PlazaPuck.jpg"},
                new ObiektSportowy() {ObiektSportowyID = 3 , KategoriaID = 3 , AdresID = 4 , Nazwa = "BoiskoHaloweWSzkole",
                DataDodania = DateTime.Now.Date , OpisObiektu = "Boisko czynne od 16-19",
                MaksymalnaLiczbaMiejsc = 20 , MinimalnaLiczbaMiejsc = 10 , Szerokosc = 25 , Dlugosc = 25,
                NazwaPlikuObrazka = "Hala.jpg"},
                 new ObiektSportowy() {ObiektSportowyID = 4 , KategoriaID = 3 , AdresID = 4 , Nazwa = "Boisko4",
                DataDodania = DateTime.Now.Date , OpisObiektu = "Boisko Jakies tam",
                MaksymalnaLiczbaMiejsc = 15 , MinimalnaLiczbaMiejsc = 17 , Szerokosc = 12 , Dlugosc = 6,
                NazwaPlikuObrazka = "Boisko3.jpg"},
                  new ObiektSportowy() {ObiektSportowyID = 3 , KategoriaID = 3 , AdresID = 4 , Nazwa = "Boisko5",
                DataDodania = DateTime.Now.Date , OpisObiektu = "Boisko Bogdana",
                MaksymalnaLiczbaMiejsc = 8 , MinimalnaLiczbaMiejsc = 23 , Szerokosc = 16 , Dlugosc = 5,
                NazwaPlikuObrazka = "Boisko4.jpg"},
            };
            obiektySportowe.ForEach(a => context.ObiektySportowe.AddOrUpdate(a));
            context.SaveChanges();

            DateTime now = DateTime.Now;

            var spotkania = new List<Spotkanie>
            {
                new Spotkanie() {SpotkanieID = 1 , ObiektSportowyID = 1 , GodzinaRozpoczecia = "12:20",
                                GodzinaZakonczenia = "23:22" , DataDodania = now, OpisSpotkania = "Tylko Dorosli",
                                Cena = 0, DataSpotkania = DateTime.Now, LiczbaMiejscZajetych = 2, GlownyObrazek="Obrazek1.jpg"},
                new Spotkanie() {SpotkanieID = 2 , ObiektSportowyID = 2 , GodzinaRozpoczecia = "13:12",
                                GodzinaZakonczenia = "14:50" , DataDodania = now, OpisSpotkania = "Tylko Dzieci do lat 12",
                                Cena = 123, DataSpotkania = DateTime.Now, LiczbaMiejscZajetych = 1, GlownyObrazek = "Obrazek2.jpg"},
                new Spotkanie() {SpotkanieID = 3 , ObiektSportowyID = 2 , GodzinaRozpoczecia = "14:12",
                                GodzinaZakonczenia = "16:50" , DataDodania = now, OpisSpotkania = "Jakis spotkanie",
                                Cena = 564, DataSpotkania = DateTime.Now, LiczbaMiejscZajetych = 3, GlownyObrazek = "Obrazek3.jpg"},
                new Spotkanie() {SpotkanieID = 4 , ObiektSportowyID = 1 , GodzinaRozpoczecia = "17:12",
                                GodzinaZakonczenia = "20:50" , DataDodania = now, OpisSpotkania = "Spotkanie4",
                                Cena = 5642, DataSpotkania = DateTime.Now, LiczbaMiejscZajetych = 0, GlownyObrazek = "Obrazek4.jpg"},
                new Spotkanie() {SpotkanieID = 5 , ObiektSportowyID = 2 , GodzinaRozpoczecia = "10:12",
                                GodzinaZakonczenia = "23:25" , DataDodania = now, OpisSpotkania = "Kolejne spotkanie6",
                                Cena = 55, DataSpotkania = DateTime.Now, LiczbaMiejscZajetych = 12, GlownyObrazek = "Obrazek5.jpg"},
            };
            spotkania.ForEach(a => context.Spotkania.AddOrUpdate(a));
            context.SaveChanges();
        }

        public static void SeedUzytkownicy(ZagrajZNamiContext db)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            const string name = "admin@zagrajznami.pl";
            const string password = "P@ssw0rd";
            const string roleName = "Admin";

            var user = userManager.FindByName(name);
            if (user == null)
            {
                user = new ApplicationUser { UserName = name, Email = name, DaneUzytkownika = new DaneUzytkownika() };
                var result = userManager.Create(user, password);
            }

            // utworzenie roli Admin jeśli nie istnieje 
            var role = roleManager.FindByName(roleName);
            if (role == null)
            {
                role = new IdentityRole(roleName);
                var roleresult = roleManager.Create(role);
            }

            // dodanie uzytkownika do roli Admin jesli juz nie jest w roli
            var rolesForUser = userManager.GetRoles(user.Id);
            if (!rolesForUser.Contains(role.Name))
            {
                var result = userManager.AddToRole(user.Id, role.Name);
            }
        }
    }
}
