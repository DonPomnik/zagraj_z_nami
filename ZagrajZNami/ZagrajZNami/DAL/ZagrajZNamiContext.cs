﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using ZagrajZNami.Models;

namespace ZagrajZNami.DAL
{
    public class ZagrajZNamiContext : IdentityDbContext<ApplicationUser>
    {
        //base(Connection String) ! W celu popraw patrz plik Web.config + <connectionString>
        public ZagrajZNamiContext() : base("ZagrajZNamiContext")
        {

        }

        //static ZagrajZNamiContext()
        //{
        //    Database.SetInitializer<ZagrajZNamiContext>(new ZagrajZNamiInitializer());
        //}

        public static ZagrajZNamiContext Create()
        {
            return new ZagrajZNamiContext();
        }

        public DbSet<Adres> Adresy { get; set; }
        public DbSet<Kategoria> Kategorie { get; set; }
        public DbSet<ObiektSportowy> ObiektySportowe { get; set; }
        public DbSet<Spotkanie> Spotkania { get; set; }
        public DbSet<Zdjecie> Zdjecia { get; set; }

        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}