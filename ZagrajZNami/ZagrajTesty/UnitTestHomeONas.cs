﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZagrajZNami.Controllers;
using ZagrajZNami.DAL;

namespace ZagrajTesty
{
    [TestClass]
    public class UnitTestHomeONas
    {
        private ZagrajZNamiContext db = new ZagrajZNamiContext();

        [TestMethod]
        public void TestMethod1()
        {
            HomeController con = new HomeController();
            ViewResult result = con.ONas() as ViewResult;

            Assert.AreEqual("onas", result.ViewName);
        }
    }
}
