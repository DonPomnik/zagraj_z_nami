﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZagrajZNami.Controllers;
using ZagrajZNami.DAL;
using ZagrajZNami.Models;

namespace ZagrajTesty
{
    [TestClass]
    public class UniTestHomeBoiskaNazwa
    {
        private ZagrajZNamiContext db = new ZagrajZNamiContext();


        [TestMethod]
        public void TestMethod1()
        {
            HomeController con = new HomeController();
            ViewResult result = con.Boiska() as ViewResult;
            List<ObiektSportowy> obiektyZWidoku = result.ViewData.Model as List<ObiektSportowy>;
            Assert.AreEqual(db.ObiektySportowe.ToList().First().Nazwa, obiektyZWidoku.First().Nazwa);
        }
    }
}
