﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZagrajZNami.Controllers;
using ZagrajZNami.DAL;

namespace ZagrajTesty
{
    [TestClass]
    public class UnitTestAccountLogin
    {
        private ZagrajZNamiContext db = new ZagrajZNamiContext();
        [TestMethod]
        public void TestMethod1()
        {
            AccountController con = new AccountController();
            ViewResult result = con.Login("test") as ViewResult;
            Assert.AreEqual("login", result.ViewName);
        }
    }
}
