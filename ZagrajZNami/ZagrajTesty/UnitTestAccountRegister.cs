﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZagrajZNami.Controllers;

namespace ZagrajTesty
{
    [TestClass]
    public class UnitTestAccountRegister
    {
        [TestMethod]
        public void TestMethod1()
        {
            AccountController con = new AccountController();
            ViewResult result = con.Register() as ViewResult;
            Assert.AreEqual("register", result.ViewName);
        }
    }
}
