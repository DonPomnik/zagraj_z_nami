﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZagrajZNami.Controllers;
using ZagrajZNami.DAL;
using ZagrajZNami.Models;

namespace ZagrajTesty
{
    [TestClass]
    public class UnitTestAccountResetPassword2
    {
        private ZagrajZNamiContext db = new ZagrajZNamiContext();

        [TestMethod]
        public void TestMethod1()
        {

            AccountController con = new AccountController();
            ViewResult result = con.ResetPassword("test") as ViewResult;
            Assert.AreEqual("resetpassword2", result.ViewName);
        }
    }
}
