﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZagrajZNami.Controllers;
using ZagrajZNami.DAL;
using ZagrajZNami.Models;

namespace ZagrajTesty
{
    [TestClass]
    public class UnitTest1
    {
        private ZagrajZNamiContext db = new ZagrajZNamiContext();


        [TestMethod]
        public void TestMethod1()
        {
            SpotkanieController con = new SpotkanieController();
            ViewResult result = con.Index() as ViewResult;
            Assert.AreNotEqual(result, null);
        }
        [TestMethod]
        public void Tesh()
        {
            SpotkanieController con = new SpotkanieController();
            ViewResult result = con.Index() as ViewResult;
            List<Spotkanie> obiektyZWidoku = result.ViewData.Model as List<Spotkanie>;
            Assert.AreEqual(db.Spotkania.ToList().First().SpotkanieID, obiektyZWidoku.First().SpotkanieID);
        }
        [TestMethod]
        public void Tes2()
        {
            HomeController con = new HomeController();
            ViewResult result = con.Boiska() as ViewResult;
            List<ObiektSportowy> obiektyZWidoku = result.ViewData.Model as List<ObiektSportowy>;
            Assert.AreEqual(db.ObiektySportowe.ToList().First().AdresID, obiektyZWidoku.First().AdresID);
        }
    }
}
