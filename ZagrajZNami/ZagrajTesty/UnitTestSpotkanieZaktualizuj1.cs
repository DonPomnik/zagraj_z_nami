﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZagrajZNami.Controllers;
using ZagrajZNami.DAL;
using ZagrajZNami.Models;

namespace ZagrajTesty
{
    [TestClass]
    public class UnitTestSpotkanieZaktualizuj1
    {
        private ZagrajZNamiContext db = new ZagrajZNamiContext();
        [TestMethod]
        public void TestMethod1()
        {
            SpotkanieController con = new SpotkanieController();
            ViewResult result = con.Zaktualizuj("DataW") as ViewResult;
            List<Spotkanie> obiektyZWidoku = result.ViewData.Model as List<Spotkanie>;
            Assert.AreEqual(db.Spotkania.ToList().First().DataDodania, obiektyZWidoku.First().DataDodania);
            
        }
    }
}
