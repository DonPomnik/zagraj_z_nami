﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZagrajZNami.Controllers;
using ZagrajZNami.DAL;
using ZagrajZNami.Models;

namespace ZagrajTesty
{
    [TestClass]
    public class UnitTestSpotkanieDataSpotkania
    {
        private ZagrajZNamiContext db = new ZagrajZNamiContext();

        [TestMethod]
        public void TestMethod1()
        {
            SpotkanieController con = new SpotkanieController();
            ViewResult result = con.Index() as ViewResult;
            List<Spotkanie> obiektyZWidoku = result.ViewData.Model as List<Spotkanie>;
            Assert.AreEqual(db.Spotkania.ToList().First().DataSpotkania, obiektyZWidoku.First().DataSpotkania);
        }
    }
}
