﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZagrajZNami.Controllers;

namespace ZagrajZNami.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            HomeController hm = new HomeController();
            var result = hm.Index() as ViewResult;
            Assert.AreNotEqual(result, null);
        }
    }
}
